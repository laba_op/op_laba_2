#ifndef LOGIC_H
#define LOGIC_H
#include "struct.h"

void Clear(csv_T* csv_t);
void getRows(csv_T* csv_t);
void getCols(csv_T* csv_t);
//void getData(csv_T* csv_t);
void calculateMetrics(csv_T* csv_t);
void calculateMediana(csv_T* csv_t);
bool isDouble(csv_T* csv_t, std::string str);
void getLines(csv_T* csv_t);


#endif // LOGIC_H
