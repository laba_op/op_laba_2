#ifndef STRUCT_H
#define STRUCT_H
#include <iostream>
#include <list>

struct csv_T{
    std::string filename;
    std::string region;
    int column = -1;
    int rows;
    int cols;
    int rows_region;
    std::list<std::string> headers;
    double metrics[3];
    std::string error = "";
    std::string line;
    std::list<std::string> data;
    std::list<double> years;
    std::list<double> values;
    int rows_index;
    int cols_year;
};

#endif // STRUCT_H
