#include "mainwindow.h"
#include "ui_mainwindow.h"
#define INDENT 50
#define SPACE 20
#define ARROW_SIZE 15
#define LINE_SIZE 5
#define TEXT_OFFSET_1 10
#define TEXT_OFFSET_2 30

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tw_Data->setEditTriggers(QAbstractItemView::NoEditTriggers);
}


MainWindow::~MainWindow()
{
    Clear(&csv_t);
    csv_t.data.clear();
    csv_t.values.clear();
    delete ui;
}


void MainWindow::on_btn_OF_clicked()
{
    Clear(&csv_t);
    clearTable();
    QString way;
    way = QFileDialog::getOpenFileName(this, "Выбор csv файла", "C:/csv files", "CSV File (*.csv)");
    if(way != ""){
        ui->lbl_FN->setText(way);
        csv_t.filename = way.toStdString();
        getCols(&csv_t);
        getRows(&csv_t);
        showData(0);
    }
    else{
        csv_t.error = "File not chosen";
        check_and_clear();
    }
}


void MainWindow::showData(int index)
{
    for(int i = 1; i < csv_t.rows; i++){
        csv_t.rows_index = i;
        doOperation(load, &csv_t);
        loadData();
    }
    if(ui->tw_Data->rowCount() == 0 && index == 1){
        csv_t.error = "incorrect name of region";
    }
    check_and_clear();
}


QStringList MainWindow::ConvertRowToQTFormat(std::list<std::string> headers, int cols)
{
    QStringList list = {};
    for(int i = 0; i < cols; i++){
        list.append(QString::fromStdString(std::to_string(i) + "." + headers.front()));
        if(headers.front() == "region"){
            csv_t.rows_region = i;
        }
        else if(headers.front() == "year"){
            csv_t.cols_year = i;
        }
        headers.pop_front();
    }
    return list;
}


void MainWindow::on_btn_LD_clicked()
{
    if(csv_t.filename != ""){
        ui->tw_Data->setColumnCount(0);
        ui->tw_Data->clearContents();
        ui->tw_Data->setRowCount(0);
        csv_t.column = ui->le_col->text().toInt();
        csv_t.region = ui->le_region->text().toStdString();
        if (csv_t.region != "" && ui->lbl_FN->text() != ""){
                showData(1);
        }
        else{
            csv_t.error = "Input region or open file";
            showData(0);
        }
    }
    else
        csv_t.error = "File not chosen";
    check_and_clear();
}


void MainWindow::loadData()
{
    QStringList list;
    for(std::string item : csv_t.data){
        list.append(QString::fromStdString(item));
    }
    if (ui->tw_Data->columnCount() == 0) {
        ui->tw_Data->setColumnCount(csv_t.cols);
        QStringList QColumns = ConvertRowToQTFormat(csv_t.headers, csv_t.cols);
        ui->tw_Data->setHorizontalHeaderLabels(QColumns);
        ui->tw_Data->setRowCount(0);
    }
    if(csv_t.region == "" || list.at(csv_t.rows_region) == QString::fromStdString(csv_t.region)){
        ui->tw_Data->setRowCount(ui->tw_Data->rowCount() + 1);
        for (int j = 0; j < csv_t.cols; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem();
            item->setData(Qt::DisplayRole, list.at(j).toDouble());
            item->setText(list.at(j));
            ui->tw_Data->setItem(ui->tw_Data->rowCount() - 1, j, item);
        }
    }
    csv_t.data.clear();
}


void MainWindow::on_btn_CaD_clicked()
{
    if(isDouble(&csv_t, ui->le_col->text().toStdString()) && !ui->le_col->text().contains('.') && !ui->le_col->text().contains(','))
        csv_t.column = ui->le_col->text().toInt();
    else
        csv_t.column = -1;
    if(csv_t.filename != ""){
        if(csv_t.column >= 0 && csv_t.column < csv_t.cols){
            getData();
            if(Check() == 1){
                ui->lbl_Result->setText("max: " + QString::number(csv_t.metrics[0], 'g', 5) + " (red)\n" + "min: " + QString::number(csv_t.metrics[1], 'g', 5) + " (blue)\n"+ "mediana: " + QString::number(csv_t.metrics[2], 'g', 5) + " (green)");
                Fill_Graph();
            }
        }
        else{
            csv_t.error = "Enter correct number of column";
        }
    }
    else
        csv_t.error = "File not chosen";
    check_and_clear();
    csv_t.values.clear();
    csv_t.years.clear();
    csv_t.data.clear();
}


void MainWindow::getData()
{
    for (int i = 0; i < ui->tw_Data->rowCount(); ++i){
        QTableWidgetItem* item = ui->tw_Data->item(i, csv_t.column);
        QTableWidgetItem* item_1 = ui->tw_Data->item(i, csv_t.cols_year);
        if (csv_t.region == "" || (ui->tw_Data->item(i, csv_t.rows_region)->text() == QString::fromStdString(csv_t.region))){
            if(item->text() != "" && isDouble(&csv_t, item->text().toStdString())){
                csv_t.values.push_back(item->text().toDouble());
            }
            if(item->text() != "" && item_1->text() != "" && isDouble(&csv_t, item_1->text().toStdString()) && !item_1->text().contains(".")){
                csv_t.years.push_back(item_1->text().toDouble());
            }
         }
    }
    if(Check() == 1){
        if (csv_t.values.size() == 0)
            csv_t.error = "No such data";
        else
            doOperation(calculate, &csv_t);
    }
}

void MainWindow::Fill_Graph()
{
    QVector<double> x = ConvertToQVector(csv_t.years);
    QVector<double> y = ConvertToQVector(csv_t.values);
    QVector<double> x_2 = x;
    QPixmap pix;
    pix.load("C:/Users/vovap/OneDrive/Документы/laba_2/fon.jpg");
    ui->lbl_graph->setPixmap(pix);
    QPainter painter(&pix);
    Set_Graph(pix, painter);
    double x_step = (pix.width()-INDENT-SPACE-TEXT_OFFSET_1-TEXT_OFFSET_2)/(csv_t.years.back()-csv_t.years.front());
    double y_step = (pix.height()-INDENT-2*SPACE-TEXT_OFFSET_1-TEXT_OFFSET_2)/(csv_t.metrics[0]-csv_t.metrics[1]);
    Draw_y(pix, painter, y_step);
    Draw_x(pix, painter, x, x_step);
    QPen pen;
    pen.setWidth(5);
    painter.setPen(pen);
    for(int i = 0; i < csv_t.values.size(); i++){
        if(y[i] == csv_t.metrics[0])
            pen.setColor(Qt::red);
        else if(y[i] == csv_t.metrics[1])
            pen.setColor(Qt::blue);
        else if((y.size() % 2 == 1 && i == (int)y.size()/2) || (y.size() % 2 == 0 && ((i == (int)y.size()/2 - 1) || i == (int)y.size()/2)))
            pen.setColor(Qt::green);
        else
            pen.setColor(Qt::black);
        painter.setPen(pen);
        painter.drawPoint(SPACE + INDENT + x_step*(x_2[i]-x_2[0]), pix.height()-SPACE - INDENT-((int)(y_step*(y[i]-csv_t.metrics[1]))));
    }
    ui->lbl_graph->setPixmap(pix);
}

void MainWindow::Set_Graph(QPixmap pix, QPainter &painter){
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(Qt::black);
    painter.drawLine(INDENT, pix.height() - INDENT, pix.width(), pix.height() - INDENT);
    painter.drawLine(INDENT, 0, INDENT, pix.height()-INDENT);
    painter.drawText(pix.width()-TEXT_OFFSET_2, pix.height()-SPACE, "years");
    painter.drawText(TEXT_OFFSET_1, SPACE, "data");
}



void MainWindow::Draw_x(QPixmap pix, QPainter &painter, QVector<double>& x, double x_step){
    auto x_1 = std::unique(x.begin(), x.end());
    x.erase(x_1, x.end());
    for(int i = 0; i < x.size(); i++){
        painter.drawLine(SPACE + INDENT + x_step*(x[i]-x[0]), pix.height() - (INDENT-LINE_SIZE), SPACE + INDENT + x_step*(x[i]-x[0]), pix.height() - (INDENT+LINE_SIZE));
        if(i%2==0){
            painter.drawText(SPACE + INDENT + x_step*(x[i]-x[0]), pix.height()-SPACE, QString::number(x[i]));
        }
    }
    painter.drawLine(pix.width()-ARROW_SIZE, pix.height()-(INDENT-ARROW_SIZE), pix.width(), pix.height()-INDENT);
    painter.drawLine(pix.width()-ARROW_SIZE, pix.height()-(INDENT+ARROW_SIZE), pix.width(), pix.height()-INDENT);
}


void MainWindow::Draw_y(QPixmap pix, QPainter &painter, double y_step){
    painter.drawText(TEXT_OFFSET_1, TEXT_OFFSET_2 + SPACE, QString::number(csv_t.metrics[0]));
    painter.drawLine(INDENT-LINE_SIZE, TEXT_OFFSET_2 + SPACE, INDENT+LINE_SIZE, TEXT_OFFSET_2 + SPACE);
    painter.drawText(TEXT_OFFSET_1, pix.height()-INDENT-SPACE, QString::number(csv_t.metrics[1]));
    painter.drawLine(INDENT-LINE_SIZE, pix.height()-INDENT-SPACE, INDENT+LINE_SIZE, pix.height()-INDENT-SPACE);
    painter.drawText(TEXT_OFFSET_1, pix.height()-INDENT-SPACE-((int)(y_step*(csv_t.metrics[2]-csv_t.metrics[1]))), QString::number(csv_t.metrics[2]));
    painter.drawLine(INDENT-LINE_SIZE, pix.height()-INDENT-SPACE-((int)(y_step*(csv_t.metrics[2]-csv_t.metrics[1]))), INDENT+LINE_SIZE, pix.height()-INDENT-SPACE-((int)(y_step*(csv_t.metrics[2]-csv_t.metrics[1]))));
    painter.drawLine(INDENT-ARROW_SIZE, ARROW_SIZE, INDENT, 0);
    painter.drawLine(INDENT+ARROW_SIZE, ARROW_SIZE, INDENT, 0);
}


QVector<double> MainWindow::ConvertToQVector(std::list<double> a)
{
    QVector<double> b;
    for(double it:a){
        b.push_back(it);
    }
    return b;
}


void MainWindow::clearTable()
{
    ui->tw_Data->setColumnCount(0);
    ui->tw_Data->setRowCount(0);
    ui->lbl_Result->setText("");
    ui->lbl_FN->setText("");
    ui->le_col->setText("");
    ui->le_region->setText("");
    ui->lbl_graph->clear();
}


int MainWindow::Check()
{
    int flag = 1;
    if(csv_t.error != ""){
        flag = 0;
    }
    return flag;
}


void MainWindow::message()
{
    QMessageBox msg;
    msg.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    msg.setIcon(QMessageBox::Warning);
    msg.setText(QString::fromStdString(csv_t.error));
    msg.setStandardButtons(QMessageBox::Ok);
    msg.setStyleSheet("QLabel{background-color: rgb(255, 255, 255); color: rgb(255, 0, 0);}");
    if(msg.exec() == QMessageBox::Ok)
        msg.close();
}


void MainWindow::check_and_clear()
{
    if(Check() == 0){
        message();
        if(csv_t.error == "incorrect name of region"){
            csv_t.error = "";
            Clear(&csv_t);
            clearTable();
            ui->lbl_FN->setText(QString::fromStdString(csv_t.filename));
            getCols(&csv_t);
            showData(0);
        }
        csv_t.error = "";
    }
}


void MainWindow::on_le_region_textChanged(const QString &arg1)
{
    ui->tw_Data->setRowCount(0);
    ui->tw_Data->setColumnCount(0);
    ui->lbl_Result->setText("Metrics");
    ui->lbl_graph->clear();
//    ui->w_graph->clearPlottables();
//    ui->w_graph->clearGraphs();
//    ui->w_graph->replot();
}


void MainWindow::on_le_col_textChanged(const QString &arg1)
{
    ui->lbl_Result->setText("Metrics");
    ui->lbl_graph->clear();
//    ui->w_graph->clearPlottables();
//    ui->w_graph->clearGraphs();
//    ui->w_graph->replot();
}
