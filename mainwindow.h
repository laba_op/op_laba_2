#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "struct.h"
#include "entry_point.h"
#include <fstream>
#include <QStringList>
#include <QMessageBox>
#include <QFileDialog>
#include <QVector>
#include <QPixmap>
#include <QPainter>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_LD_clicked();
    void on_btn_CaD_clicked();
    void on_btn_OF_clicked();
    void on_le_region_textChanged(const QString &arg1);
    void on_le_col_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    csv_T csv_t;
    void clearTable();
    void getData();
    void message();
    int Check();
    void check_and_clear();
    void loadData();
    void showData(int index);
    void Fill_Graph();
    void Set_Graph(QPixmap pix, QPainter &painter);
    void Draw_x(QPixmap pix, QPainter &painter, QVector<double>& x, double x_step);
    void Draw_y(QPixmap pix, QPainter &painter, double y_step);
    QVector<double> ConvertToQVector(std::list<double> a);
    QStringList ConvertRowToQTFormat(std::list<std::string> headers, int cols);
};
#endif // MAINWINDOW_H
