#include "logic.h"
#include <iostream>
#include <fstream>
#include <iterator>
#include <list>
#include <QDebug>

void Clear(csv_T* csv_t)
{
    csv_t->cols = 0;
    csv_t->column = -1;
    //csv_t->data = {};
    //csv_t->filename = "";
    csv_t->headers.clear();
    csv_t->region = "";
    csv_t->rows = 0;
    csv_t->rows_region = 0;
    csv_t->error = "";
    csv_t->years.clear();
    csv_t->values.clear();
    csv_t->data.clear();
}


void getLines(csv_T* csv_t)
{
    std::ifstream in(csv_t->filename);
    std::string line;
    getline(in, line);
    int j = 0;
    while(j < csv_t->rows_index && getline(in, csv_t->line)){
        j++;
    }
        std::string word;
        int i = 0;
        do {
            if(csv_t->line[i] == ',' || csv_t->line[i] == '\0'){
                csv_t->data.push_back(word);
                word = "";
            }
            else
                word += csv_t->line[i];
        } while(csv_t->line[i++] != '\0');
    in.close();
}


void calculateMetrics(csv_T* csv_t)
{
    double min = csv_t->values.front(), max = csv_t->values.front();
    for(double it : csv_t->values){
        if(it < min)
            min = it;
        if(it > max)
            max = it;
    }
    csv_t->metrics[0] = max;
    csv_t->metrics[1] = min;
    calculateMediana(csv_t);
}


void calculateMediana(csv_T* csv_t)
{
    double mediana = 0;
    std::list<double> values = csv_t->values;
    values.sort();
    int i = 0;
    for (auto it : csv_t->values){
        if(values.size() % 2 == 1){
            if(i == (int)(values.size() / 2)){
                mediana = 2*it;
            }
        }
        else if(values.size() % 2 == 0 && (i == (int)(values.size()/2 - 1) || i == (int)(values.size()/2))){
            mediana += it;
        }
        i++;
    }
    csv_t->metrics[2] = mediana / 2;
}

bool isDouble(csv_T* csv_t, std::string str)
{
    try
    {
        std::stod(str);
        return true;
    }
    catch (...)
    {
        csv_t->error = "Incorrect number of column";
        return false;
    }
}


void getRows(csv_T* csv_t)
{
    std::string line;
    std::ifstream in(csv_t->filename);
    int rows = 0;
    if (in.is_open())
    {
        while (getline(in, line))
        {
            ++rows;
        }
        csv_t->rows = rows;
    }
    in.close();
}


void getCols(csv_T* csv_t)
{
    std::string line;
    std::ifstream in(csv_t->filename);
    std::list<std::string> headers;
    if (in.is_open())
    {
        getline(in, line);
        int i = 0;
        std::string word;
        do{
            if(line[i] == ',' || line[i] == '\0'){
                headers.push_back(word);
                word = "";
            }
            else
                word += line[i];

        }while(line[i++] != '\0');
        csv_t->cols = headers.size();
        csv_t->headers = headers;
    }
    in.close();
}
