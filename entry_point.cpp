#include "entry_point.h"
//#include "logic.h"

void doOperation(Operation operation, csv_T* csv_t) {
    switch (operation) {
    case load:
        getLines(csv_t);
        break;

    case calculate:
        calculateMetrics(csv_t);
        break;

    default:
        csv_t->error = "incorrect function";
    }
};
